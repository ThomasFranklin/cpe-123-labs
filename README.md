This is a collection of Lab exercises I completed for CPE 123, taken Fall Quarter, 2014.

**Lab 1:**
A set of exercises that familiarized us with the syntax of Racket.

**Lab 3:**
 Introduction to the rsound package and conditional statements.

**Lab 4:**
 Manipulating sound clips using the rsound package, as well as an introduction to Racket's big bang function.

**Lab 5:**
 Introduction to lists using the cons and empty structure.

**Lab 6:**
 Using lists, structures, and pstream to play a song from a list of notes.